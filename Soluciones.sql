﻿  USE ciclistas



/*SELECT dorsal
  FROM 
  (SELECT dorsal FROM ciclista); **/

-- Ejercicio1
-- Listar las edades de los ciclistas (sin repetidos)
SELECT DISTINCT
  c.edad -- proyeccion
  FROM
  ciclista c; --tabla que utilizo
/*
Consulta 2:
  Listas las edades de los ciclistas de Artiach

 */  
  
  
  SELECT DISTINCT 
  ciclista c  -- tabla (1)
    WHERE nomequipo='Artiach'; -- seleccion (2)
    
  /*
Consulta 3:
  Listas las edades de los ciclistas de Artiach o de Amore Vita

 */  
  -- Solución con el operador OR
  SELECT DISTINCT
    c.edad

    FROM
    ciclista c
    WHERE 
    (nomequipo='Artiach'
    OR
    nomequipo='Amore Vita';

-- Solución con el operador extendido IN

   SELECT DISTINCT
      c.edad
    FROM
      ciclista c
    WHERE 
      (nomequipo= IN  ('Artiach','Amore Vita'));

  -- operador de union

SELECT DISTINCT 
    c.edad
  FROM
    ciclista c
  WHERE 
    c.nomequipo='Artiach'
  UNION
SELECT DISTINCT 
    c.edad
  FROM
    ciclista c
  WHERE c.nomequipo='Amore Vita';


SELECT * FROM
SELECT c.nomequipo FROM ciclista c WHERE c.edad=30 c1
NATURAL JOIN
SELECT c.nomequipo FROM ciclista c WHERE c.edad=31 c2;

SELECT * FROM ciclista c
  ORDER BY edad DESC, c.nombre ASC;

/** Consulta 4 
Listar los dorsales de los ciclistas cuya edad sea 
menor que 25 o mayor que 30.**/

-- or

SELECT c.dorsal 
FROM ciclista c 
WHERE 
c.edad<25 OR c.edad>30;

-- union

  -- c1 listar ciclistas menores de 25

  SELECT c.dorsal FROM  ciclista c WHERE c.edad<25;
    
   -- c2 listar ciclistas mayores de 30

  SELECT c.dorsal FROM  ciclista c WHERE c.edad>30;

  -- final

  SELECT c.dorsal FROM  ciclista c WHERE c.edad<25
  UNION
  SELECT c.dorsal FROM  ciclista c WHERE c.edad>30;

 -- between
 
 SELECT c.dorsal 
    FROM  ciclista c 
    WHERE (c.edad NOT BETWEEN 25 AND 30);


  -- Operadores cadenas
  -- char_Length 

SELECT CHAR_LENGTH("la casña"), LENGTH("lña casa");
SELECT CHAR_LENGTH(c.nombre), c.nombre
FROM ciclista c;

SELECT CHAR_LENGTH(c.nombre) longitud, c.nombre;

SELECT date(NOW()),CURRENT_DATE();

-- funciones totales

SELECT * FROM ciclista c;
SELECT COUNT(edad), COUNT(*) FROM ciclista c;
SELECT COUNT(*)-COUNT(edad) FROM ciclista c;
SELECT DISTINCT edad FROM ciclista c;

SELECT COUNT(*) FROM
  (SELECT DISTINCT edad FROM ciclista c) c1;


SELECT COUNT(*) FROM


SELECT COUNT(DISTINCT edad) FROM ciclista c;

/** 
  5.-Listar los dorsales de los ciclistas 
  cuya edad esté entre 28 y 32 y además que sólo sean de Banesto.**/
  
  SELECT c.dorsal FROM ciclista c
    WHERE ((c.edad BETWEEN 28 AND 32) AND (c.nomequipo='Banesto'));

/** con el operador intersección**/
  
  
  SELECT * FROM 
  (SELECT c.dorsal 
    FROM ciclista c 
    WHERE c.edad BETWEEN 28 AND 30) c1
  JOIN  
  (SELECT c.dorsal 
    FROM ciclista c 
    WHERE nomequipo='Banesto') c2
  c1.dorsal=c2.dorsal;

/** 
  6.-Indícame el nombre de los ciclistas 
  que el nº de caracteres del nombre sea mayor a 8 **/
  
  SELECT * FROM ciclista c 
    WHERE CHAR_LENGTH() c.nombre>8;

/** 
  7.-Lístame el nombre y el dorsal de todos ciclistas mostrando 
  un campo nuevo denominado nombre mayúsculas 
  que debe mostrar el nombre en mayúsculas **/
  SELECT c.dorsal,UPPER(c.nombre) 
    FROM ciclista c;

  /**
    8.-Listar todos los ciclistas 
    que han llevado el maillot MGE (amarillo) en alguna etapa.**/

SELECT DISTINCT c.dorsal 
  FROM ciclista c 
  WHERE c.dorsal='MGE';
 /** 
   9.-Listar el nombre de los puertos 
   cuya altura sea mayor que 1500.**/

  SELECT p.nompuerto 
    FROM puerto p
    WHERE p.altura>1500;
/**
  10.-Listar el dorsal de los ciclistas 
    que hayan ganado algún puerto 
    cuya pendiente sea mayor que 8 
    o cuya altura esté entre 1800 y 3000.**/

-- Solucion con operador OR 
 
 SELECT DISTINCT  
    FROM puerto p
    WHERE  p.altura BETWEEN 1800 AND 3000
    OR p.pendiente>8;
-- Solucion con operador UNION 

  SELECT 
    DISTINCT p.dorsal 
  FROM 
    puerto p 
  WHERE 
    p.pendiente>8 
  UNION
  SELECT 
    DISTINCT p.dorsal 
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1800 AND 3000;

/**
  11.- Listar el dorsal de los ciclistas 
    que hayan ganado algún puerto 
    cuya pendiente sea mayor que 8 
    y cuya altura esté entre 1800 y 3000.**/
SELECT DISTINCT p.dorsal  
    FROM puerto p
    WHERE  p.altura BETWEEN 1800 AND 3000
    AND p.pendiente>8;





/** Nº de ciclistas que hay.**/
  SELECT COUNT(*) 
  FROM  ciclista c;
/** Nº de ciclistas que hay en el equipo Banesto.**/
  SELECT COUNT(*) 
    FROM ciclista c
    WHERE c.nomequipo='Banesto'; 
/** La edad media de los ciclistas**/
  SELECT AVG(c.edad) 
    FROM ciclista c;
  /** La edad media de los ciclistas de Banesto.**/ 
    SELECT AVG(c.edad) 
      FROM ciclista c
      WHERE c.nomequipo='Banesto';
/** La edad media de los ciclistas por cada equipo**/
    SELECT c.nomequipo, AVG(c.edad) 
      FROM ciclista c
      GROUP BY c.nomequipo;
/** El nº de ciclistas por equipo. **/

SELECT COUNT(*),c.nomequipo
  FROM ciclista c
  GROUP BY c.nomequipo; 
/** El nº total de puertos. **/
SELECT COUNT(*)
  FROM puerto p;
/** El nº total de puertos mayores de 1500 **/
  SELECT COUNT(*) 
    FROM puerto p
    WHERE p.altura>1500;
/** listar el nombre de los equipos 
  que tengan más de 4 ciclistas.**/

SELECT COUNT(*) numciclistas,nomequipo 
  FROM ciclista c
  GROUP BY c.nomequipo
  HAVING numciclistas>4;

/** La altura media de los puertos por etapa **/
  SELECT p.numetapa, AVG(p.altura), COUNT(*)
    FROM puerto p 
    GROUP BY p.numetapa;

  
  SELECT AVG(p.altura),p.categoria
    FROM puerto p
    GROUP BY p.categoria
    HAVING AVG(p.altura)>2000;
    

  SELECT AVG(p.altura),media,p.categoria
    FROM puerto p
    GROUP BY p.categoria
    HAVING media>2000;

  SELECT p.categoria
    FROM puerto p
    GROUP BY p.categoria
    HAVING AVG(p.altura)>2000;

  SELECT p.categoria,AVG(p.altura),COUNT(*)
    FROM puerto p
    WHERE p.altura>1500
    GROUP BY p.categoria
    HAVING AVG(p.altura)>2000;
    
  SELECT * FROM (
  SELECT p.categoria,AVG(p.altura),media,COUNT(*) numero
    FROM puerto p
    WHERE p.altura>1500
    GROUP BY p.categoria) c1
    WHERE (c1.media02)>2000;

/** Listar el nombre de los equipos 
  que tengan más de cuatro ciclistas 
  cuya edad esté entre 28 y 32. **/

  SELECT c.nomequipo
    FROM ciclista c
    WHERE c.edad BETWEEN 28 AND 32
    GROUP BY c.nomequipo
    HAVING COUNT(*)>4;
  
/** Indícame el número de etapas 
  que ha ganado cada uno de los ciclistas.**/
    
  SELECT c.dorsal,COUNT(*)
    FROM etapa e
    GROUP BY c.dorsal;

/** Indícame el dorsal de los ciclistas.**/

SELECT e.dorsal 
  FROM etapa e
  GROUP BY e.dorsal
  HAVING COUNT(*)>1;

/** Consultas de Totales: (HOJA 3)
  Listar las edades de todos los ciclistas de Banesto.**/
  
  SELECT DISTINCT c.edad 
    FROM ciclista c
    WHERE c.nomequipo='Banesto';

/** listar las edades de los ciclistas 
  que son de Banesto o de Navigare.**/
  SELECT DISTINCT c.edad 
    FROM ciclista c
    WHERE c.nomequipo='Banesto' 
    OR c.nomequipo='Navigare';   
/**Usando la claúsula IN **/
  
  SELECT DISTINCT c.edad 
    FROM ciclista c
    WHERE c.nomequipo 
    IN('Banesto','Navigare');
/** Listar el dorsal de los ciclistas 
  que son de Banesto y cuya edad está entre 25 y 32**/
    SELECT DISTINCT c.dorsal 
    FROM ciclista c 
    WHERE c.nomequipo='Banesto' AND c.edad BETWEEN 25 AND 32;
/** Listar el dorsal de los ciclistas 
  que son de Banesto y cuya edad está entre 25 ó 32**/
  SELECT DISTINCT c.dorsal 
    FROM ciclista c 
    WHERE c.nomequipo='Banesto' OR c.edad BETWEEN 25 AND 32;
/** Listar la inicial del equipo de los ciclistas 
  cuyo nombre comience por R 
  
  SELECT DISTINCT LEFT(c.nomequipo)  
    FROM ciclista c
    WHERE c.nomequipo LIKE 'R%';**/

  /** Listar el código de las etapas 
  que su salida y llegada sea en la misma población.**/
    SELECT e.numetapa 
      FROM etapa e 
      WHERE e.salida=e.llegada;
    /**Listar el código de las etapas 
      que su salida y llegada 
      no sean en la misma población 
      y que conozcamos el dorsal del ciclista 
      que ha ganado la etapa.**/
      SELECT e.numetapa 
        FROM etapa e
        WHERE e.salida <> e.llegada AND e.dorsal IS NOT NULL;
      /** Listar el nombre de los puertos 
        cuya altura entre 1000 y 2000 
        o que la altura sea mayor que 2400.**/
      SELECT p.nompuerto 
        FROM puerto p
        WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura>2400;
      /**listar el número de ciclistas 
        que hayan ganado alguna etapa**/
      SELECT COUNT(DISTINCT dorsal) nCiclistas
        FROM etapa e;
      /**Con Subconsulta**/
      SELECT COUNT(*) nCiclistas 
        FROM(          
      SELECT DISTINCT dorsal nCiclistas
        FROM etapa e) c1;
      /** Listar el número de etapas 
        que tengan puerto.**/
      SELECT COUNT(DISTINCT p.numetapa) nEtapas
        FROM puerto p;
      /**Listar el número de ciclistas 
        que hayan ganado algún puerto.**/
      SELECT COUNT(DISTINCT p.dorsal) nCiclistas 
        FROM puerto p;
      /** Indicar la altura media de los puertos**/
      SELECT AVG(p.altura)
        FROM puerto p;
      /** Listar el código de la etapa 
        con el número de puertos que tiene **/
      SELECT p.numetapa, COUNT(*) 
        FROM puerto p 
        GROUP BY p.numetapa;
      /**Indicar la altura media de los puertos.**/
      SELECT AVG(p.altura) FROM puerto p
      /**Indicar el código de etapa 
        cuya altura media de sus puertos 
        esté por encima de 1500.**/
      SELECT p.numetapa
        FROM puerto p
        HAVING AVG(p.altura)>1500;
       
      /**Con GROUP BY**/
      SELECT p.numetapa, AVG(altura) altmedia 
        FROM puerto p
        GROUP BY p.numetapa;
      /**Con Subconsulta**/
      SELECT c1.numetapa 
       FROM( 
       SELECT p.numetapa, AVG(altura) altmedia 
        FROM puerto p
        GROUP BY p.numetapa) c1
        WHERE c1.altmedia>1500;
      /**Indicar el número de etapas 
        que cumplen la condición anterior.**/
        
        SELECT p.numetapa 
          FROM puerto p
          GROUP BY p.numetapa
          HAVING AVG(p.altura)>1500;
        
        SELECT COUNT(*) nEtapas 
          FROM(
            SELECT p.numetapa 
              FROM puerto p
              GROUP BY p.numetapa
              HAVING AVG(p.altura)>1500) c1;
       /** Listar el dorsal del ciclista 
        con el número de veces 
        que ha llevado algún maillot.**/
        SELECT l.dorsal, l.código, COUNT(*)
          FROM lleva l
          GROUP BY l.dorsal, l.código; 
        /** Listar el dorsal del ciclista 
        con el çodigo del maillot 
        y cuantas veces ese ciclista 
        ha llevado ese maillot.**/
        SELECT l.dorsal,l.código, COUNT(*) numero
          FROM lleva l
          GROUP BY l.dorsal,l.código;
        /** Listar el dorsal, el código de etapa, 
          el ciclista y el número de maillots 
          que ese ciclista ha llevado en cada etapa.**/
          
          SELECT l.dorsal,l.código 
            FROM lleva l
            GROUP BY l.dorsal,l.numetapa;
          
          SELECT c1.dorsal
            FROM(
              SELECT l.dorsal,l.código 
                FROM lleva l
                GROUP BY l.dorsal,l.numetapa) c1
          INNER JOIN
            ciclista c
          ON c1.dorsal=c.dorsal;

          SELECT l.dorsal,l.numetapa, c.nombre, COUNT(*) numero
             FROM lleva l
             JOIN ciclista c
              ON l.dorsal = c.dorsal
             GROUP BY c.dorsal,l.numetapa;
/** EJERCICIOS HOJA 4
  Consulta de Combinaciones Internas**/

/** Nombre y edad de los ciclistas que han ganado etapas**/
  /** Sin optimizar**/
  
  SELECT DISTINCT c.edad, c.nombre 
    FROM etapa e1
    JOIN ciclista c ON c.dorsal = e.dorsal;
/** Optimizando Consultas
Ciclistas que han ganado etapas**/
  
  SELECT e.dorsal FROM etapa e
/**Consulta completa con JOIN**/
  
  SELECT nombre,edad
    FROM(SELECT DISTINCT dorsal FROM etapa) c1
    JOIN ciclista  USING(dorsal);
/**Consulta completa con IN**/
  
  SELECT c.nombre,c.edad 
    FROM ciclista c
    WHERE c.dorsal IN(SELECT DISTINCT e.dorsal FROM etapa e);

-- Creando vistas
  CREATE OR REPLACE VIEW c1 AS
    SELECT DISTINCT e.dorsal FROM etapa e;
  SELECT nombre,edad FROM c1 c
  JOIN ciclista c1 USING(dorsal);
  
  DROP VIEW c1;
  
  /** Nombre y edad de los ciclistas que han ganado puertos**/
    SELECT DISTINCT nombre,c.edad 
      FROM puerto p 
      JOIN ciclista c ON p.dorsal = c.dorsal;
  /** Nombre y edad de los ciclistas que hayna ganado etapas y puertos**/
    SELECT DISTINCT nombre,c.edad 
      FROM (ciclista c INNER JOIN etapa e ON c.dorsal = e.dorsal)
      INNER JOIN puerto p ON c.dorsal= p.dorsal;
/** Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa**/
    SELECT DISTINCT e.director 
    FROM equipo e INNER JOIN (ciclista c INNER JOIN etapa e1 ON c.dorsal=e1.dorsal) 
    ON e.nomequipo=c.nomequipo;
/** Dorsal y nombre de los ciclistas que hayan llevado algún maillot.**/
    SELECT l.dorsal, c.nombre 
    FROM ciclista c INNER JOIN lleva l ON c.dorsal = l.dorsal;
/** 


